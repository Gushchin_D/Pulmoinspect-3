from .utils.dataloaders import SegmentationDataGenerator, ClassificationDataGenerator
from .utils.dataloaders.utils import load_and_resize_image, save_segmented_result
from .utils.result_visualize_callback import SegmentationResultsVisualizer
from .utils.tools import (get_segm_model_and_sizes, get_class_model_and_size, check_file, check_dir,
                          image_arr_to_single_batch, predicted_mask_batch_to_image)
from .utils.training import train


def segm_train(model, images_path, masks_path, batch_size, epochs,
               checkpoints_path, out_images_path, weights_save, **model_kwargs):
    model, (inp_size, out_size) = get_segm_model_and_sizes(model, model_kwargs)
    dataset = SegmentationDataGenerator(images_path, masks_path, inp_size, out_size, batch_size)
    callbacks = []
    if out_images_path:
        callbacks.append(SegmentationResultsVisualizer(dataset, out_images_path))
    train(model, dataset, epochs, checkpoints_path, weights_save, callbacks)


def class_train(model, images_path, labels_file, batch_size, epochs, checkpoints_path, weights_save, **model_kwargs):
    model, input_size = get_class_model_and_size(model, model_kwargs)
    dataset = ClassificationDataGenerator(images_path, input_size, labels_file, batch_size)
    train(model, dataset, epochs, checkpoints_path, weights_save)


def segm_predict(model, weights_file_path, image_path, out_image_path, **model_kwargs):
    weights_file_path = check_file(weights_file_path, True)
    image_path = check_file(image_path, True)
    out_image_path = check_file(out_image_path, False)
    model, (inp_size, out_size) = get_segm_model_and_sizes(model, model_kwargs)
    model.load_weights(weights_file_path)
    image = image_arr_to_single_batch(load_and_resize_image(image_path, inp_size))
    out = model.predict(image)
    if out_image_path:
        save_segmented_result(out, out_image_path)
    return predicted_mask_batch_to_image(out)


def class_predict(model, weights_file_path, image_path, **model_kwargs):
    weights_file_path = check_file(weights_file_path, True)
    image_path = check_file(image_path, True)
    model, size = get_class_model_and_size(model, model_kwargs)
    model.load_weights(weights_file_path)
    image = image_arr_to_single_batch(load_and_resize_image(image_path, size))
    classes = model.predict(image)
    print(*classes)
    return classes
