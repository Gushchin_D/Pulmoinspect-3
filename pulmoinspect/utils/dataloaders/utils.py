from collections import Counter
from mimetypes import guess_type
import pandas as pd
from tensorflow.keras.preprocessing.image import img_to_array, load_img, save_img

from ..tools import check_dir, check_file, predicted_mask_batch_to_image


def check_choices(choices):
    if choices:
        raise FileExistsError(f'Multiple choices for: {", ".join(choices)}')


def check_and_get_images_from_dir(directory):
    directory = check_dir(directory)
    images_paths = []
    for file in directory.glob('*?.?*'):
        if (mime := guess_type(file)[0]) and mime.startswith('image/'):
            images_paths.append(file)
    choices = [name
               for name, count in Counter(f.stem for f in images_paths).most_common()
               if count > 1]
    check_choices(choices)
    return images_paths


def check_and_get_masks_from_dir(directory, images_names):
    directory = check_dir(directory)
    masks_paths = []
    choices = set()
    not_found = set()
    for name in images_names:
        if masks := filter(lambda f: guess_type(f)[0].startswith('image/'), directory.glob(name + '.?*')):
            masks = list(masks)
            if len(masks) > 1:
                choices.add(name)
            else:
                masks_paths.append(masks[0])
        else:
            not_found.add(name)
    if not_found:
        raise FileNotFoundError(f'No mask for: {", ".join(not_found)}')
    check_choices(choices)
    return masks_paths


def check_and_get_labels_from_file(file, images_names):
    file = check_file(file, must_exist=True)
    table = pd.read_csv(file)
    table.set_index(table.columns[0], inplace=True)
    if len(table.columns) < 2:
        raise ValueError('Labels should contain at least 2 classes')
    images_labels = []
    not_found = set()
    choices = set()
    for name in images_names:
        if name in table.index:
            res = table.loc[name]
            if res.ndim == 1:
                images_labels.append(res.values)
            else:
                choices.add(name)
        else:
            not_found.add(name)
    if not_found:
        raise FileNotFoundError(f'No label for: {", ".join(not_found)}')
    check_choices(choices)
    return images_labels


def load_and_resize_image(image_path, size):
    image_path = check_file(image_path, must_exist=True)
    if isinstance(size, tuple):
        size = (size, size)
    img = load_img(image_path, target_size=(size, size), color_mode='grayscale')
    return img_to_array(img)


def save_segmented_result(image, path):
    path = check_file(path, False)
    image = predicted_mask_batch_to_image(image)
    save_img(path, image)
