from .base import BaseXImageDataGenerator
from .utils import check_and_get_labels_from_file


class ClassificationDataGenerator(BaseXImageDataGenerator):
    def __init__(self, input_images_path, input_image_size, labels_file_path, batch_size):
        super().__init__(input_images_path, input_image_size, batch_size)
        self.labels = check_and_get_labels_from_file(labels_file_path, [path.stem for path in self.images_paths])

    def _get_y(self, index):
        return self.labels[index]
