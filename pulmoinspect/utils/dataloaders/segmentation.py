from .base import BaseXImageDataGenerator
from .utils import check_and_get_masks_from_dir, load_and_resize_image


class SegmentationDataGenerator(BaseXImageDataGenerator):
    def __init__(self, images_path, masks_path, input_image_size, output_image_size, batch_size):
        super().__init__(images_path, input_image_size, batch_size)
        self.masks_paths = check_and_get_masks_from_dir(masks_path, [path.stem for path in self.images_paths])
        self.output_image_size = output_image_size

    def _get_y(self, index):
        return load_and_resize_image(self.masks_paths[index], self.output_image_size)
