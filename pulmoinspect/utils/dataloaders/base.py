from math import ceil
from random import shuffle
import numpy as np

from tensorflow.keras.utils import Sequence

from .utils import check_and_get_images_from_dir, load_and_resize_image


class BaseXImageDataGenerator(Sequence):
    def __init__(self, input_images_path, input_image_size, batch_size):
        self.images_paths = check_and_get_images_from_dir(input_images_path)
        self.input_image_size = input_image_size
        self.batch_size = batch_size
        self.indexes = list(range(len(self.images_paths)))

    def on_epoch_end(self):
        shuffle(self.indexes)

    def __len__(self):
        return ceil(len(self.images_paths) / self.batch_size)

    def __getitem__(self, index):
        return self._get_batch(index)

    def _get_x(self, index):
        return load_and_resize_image(self.images_paths[index], self.input_image_size)

    def _get_y(self, index):
        raise NotImplementedError()

    def _get_batch(self, index):
        if index >= len(self):
            raise IndexError('Index out of range')
        batch_start_index = index * self.batch_size
        batch_end_index = (index + 1) * self.batch_size
        indexes = self.indexes[batch_start_index:batch_end_index]
        input_shape = self._get_x(0).shape
        output_shape = self._get_y(0).shape
        xs = np.zeros((len(indexes), *input_shape), dtype=float)
        ys = np.zeros((len(indexes), *output_shape), dtype=float)
        for arr_index, index in enumerate(indexes):
            xs[arr_index] = self._get_x(index)
            ys[arr_index] = self._get_y(index)
        return xs, ys
