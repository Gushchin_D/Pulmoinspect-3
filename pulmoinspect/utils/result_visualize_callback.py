from random import randint

from tensorflow.keras.callbacks import Callback

from .dataloaders.utils import save_segmented_result
from ..utils.tools import check_dir, image_arr_to_single_batch


class SegmentationResultsVisualizer(Callback):
    def __init__(self, data_generator, out_images_path):
        super().__init__()
        self.data_generator = data_generator
        self.out_images_path = check_dir(out_images_path)

    def on_epoch_end(self, epoch, logs=None):
        img = self.data_generator[randint(0, len(self.data_generator) - 1)][0]
        img = image_arr_to_single_batch(img[randint(0, len(img) - 1)])
        save_segmented_result(self.model.predict(img), self.out_images_path / f'{epoch}.png')
