from tensorflow.keras.callbacks import ModelCheckpoint

from ..utils.tools import check_file, check_dir


def train(model, data_generator, epochs, checkpoints_path, output_path, callbacks=None):
    if callbacks is None:
        callbacks = list()

    start_epoch = 0
    weights_path = check_file(output_path, must_exist=False)
    if checkpoints_path:
        checkpoints_path = check_dir(checkpoints_path)
        callbacks.append(ModelCheckpoint(filepath=checkpoints_path / 'weights-{epoch}.h5', save_weights_only=True))
        checks = list(sorted(checkpoints_path.glob('weights-*.h5')))
        if len(checks) > 0:
            last_checkpoint = checks[-1]
            model.load_weights(last_checkpoint)
            start_epoch = int(last_checkpoint.stem[8:])
    model.fit(data_generator, epochs=epochs, callbacks=callbacks, initial_epoch=start_epoch)
    model.save_weights(weights_path)