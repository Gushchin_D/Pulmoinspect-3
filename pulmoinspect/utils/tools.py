from pathlib import Path
from collections import namedtuple
from pkgutil import iter_modules
from importlib import import_module
from inspect import isfunction

models_dir = Path(__file__).parents[1] / 'models'
segmenters_dir = models_dir / 'segmentation'
classifiers_dir = models_dir / 'classification'


def check_dir(path):
    if isinstance(path, str):
        path = Path(path)
    elif not issubclass(type(path), Path):
        raise TypeError('path must be str or pathlib.Path object')
    if not path.exists():
        raise NotADirectoryError(f'"{path}" does not exist')
    if not path.is_dir():
        raise NotADirectoryError(f'"{path}" is not a directory')
    return path


def check_file(path, must_exist):
    if isinstance(path, str):
        path = Path(path)
    elif not issubclass(type(path), Path):
        raise TypeError('path must be str or pathlib.Path object')
    if path.exists() != must_exist:
        raise FileNotFoundError(f'File "{path}" does not exist') if must_exist else FileExistsError(
            f'File "{path}" exists')
    return path


AvailableModels = namedtuple('AvailableModels', ('segmentation', 'classification'))


def list_models():
    def filter_models(path):
        return [item.name for item in iter_modules([path])]

    return AvailableModels(filter_models(segmenters_dir), filter_models(classifiers_dir))


def import_model(model_type, model_name):
    available = list_models()
    if not hasattr(available, model_type):
        raise ValueError(f'Incorrect model type: "{model_type}"')
    if model_name not in getattr(available, model_type):
        raise ModuleNotFoundError(f'Model for {model_type} not found: "{model_name}"')
    try:
        imported = import_module(f'.models.{model_type}.{model_name}', 'pulmoinspect')
        func = getattr(imported, 'create')
        if not isfunction(func):
            raise TypeError('Model creating function is not a function')
        return func
    except ImportError:
        raise SystemError('Unable to import module')
    except AttributeError:
        raise SystemError('Unable to access model creating function')


def check_size(size, kind):
    if size[1] != size[2]:
        raise ValueError(f'Model {kind} must be square image (got {size[1]}x{size[2]})')
    return size[1]


def get_model(model_type, model, model_kwargs):
    if isinstance(model, str):
        model = import_model(model_type, model)(**model_kwargs)
    return model


def get_segm_model_and_sizes(model, model_kwargs):
    model = get_model('segmentation', model, model_kwargs)
    return model, (check_size(model.input_shape, 'input'),
                   check_size(model.output_shape, 'output'))


def get_class_model_and_size(model, model_kwargs):
    model = get_model('classification', model, model_kwargs)
    return model, check_size(model.input_shape, 'input')


def image_arr_to_single_batch(image):
    image.shape = 1, *image.shape
    return image


def predicted_mask_batch_to_image(image_batch):
    return image_batch[0, ...]
